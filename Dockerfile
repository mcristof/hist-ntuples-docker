FROM centos/python-36-centos7:latest

RUN pip install --upgrade pip && \
	pip install uproot && \
	pip install pandas && \ 
	pip install rucio-clients


COPY . /hist
WORKDIR /hist

## run jupyter notebook by default unless a command is specified
#CMD ["python", "hist_grid.py"]
#CMD ["python", "rucio_test.py"]