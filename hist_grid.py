import uproot
import numpy as np
import pandas as pd
import os
from options import *
import uproot_methods.classes.TH1

class SimpleNamespace (object):
    def __init__ (self, **kwargs):
        self.__dict__.update(kwargs)
    def __repr__ (self):
        keys = sorted(self.__dict__)
        items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))
    def __eq__ (self, other):
        return self.__dict__ == other.__dict__
    
class MakeTH1(uproot_methods.classes.TH1.Methods, list):
    def __init__(self, low, high, values, title=""):
        self._fXaxis = SimpleNamespace()
        self._fXaxis._fNbins = len(values)
        self._fXaxis._fXmin = low
        self._fXaxis._fXmax = high
        self.append(0)
        for x in values:
            self.append(float(x))
        self.append(0)            
        self._fTitle = title
        self._classname = "TH1F"    

length_checker = np.vectorize(len) 
        
mass_ll_f = np.vectorize(lambda x: (x[0] + x[1]).mass)

def compute_mll(l, data, index):
    jet4v = uproot_methods.TLorentzVectorArray.from_ptetaphi(data[('jet_pt').encode()], data[('jet_eta').encode()], data[('jet_phi').encode()], data[('jet_e').encode()])[index]    
    if l != 'elmu':
        lept4v = uproot_methods.TLorentzVectorArray.from_ptetaphim(data[(l+'_pt').encode()], data[(l+'_eta').encode()], data[(l+'_phi').encode()], 0)[index]
        mass_ll_i = mass_ll_f(lept4v)
        
        m_l1_j1 = (lept4v[:,0] + jet4v[:,0]).mass2
        m_l2_j2 = (lept4v[:,1] + jet4v[:,1]).mass2
        m_l1_j2 = (lept4v[:,0] + jet4v[:,1]).mass2
        m_l2_j1 = (lept4v[:,1] + jet4v[:,0]).mass2 
        min_m2_pos = np.argmin(np.vstack([m_l1_j1 + m_l2_j2, m_l1_j2 + m_l2_j1]).T, axis=1)
        mlj = np.sqrt(np.hstack([list(zip(m_l1_j1,m_l2_j2)), list(zip(m_l1_j2,m_l2_j1))]).reshape(len(m_l1_j1), 2, 2)[np.arange(len(min_m2_pos)),min_m2_pos])        
    else:
        el_4v = uproot_methods.TLorentzVectorArray.from_ptetaphim(data['el_pt'.encode()], data['el_eta'.encode()], data['el_phi'.encode()], 0)[index]
        mu_4v = uproot_methods.TLorentzVectorArray.from_ptetaphim(data['mu_pt'.encode()], data['mu_eta'.encode()], data['mu_phi'.encode()], 0)[index]
        mass_ll_i = (el_4v + mu_4v).mass

        m_l1_j1 = (el_4v + jet4v[:,0]).mass2
        m_l2_j2 = (mu_4v + jet4v[:,1]).mass2
        m_l1_j2 = (el_4v + jet4v[:,1]).mass2
        m_l2_j1 = (mu_4v + jet4v[:,0]).mass2         
        min_m2_pos = np.argmin(np.vstack([m_l1_j1 + m_l2_j2, m_l1_j2 + m_l2_j1]).T, axis=1)
        mlj = np.sqrt(np.hstack([list(zip(m_l1_j1,m_l2_j2)), list(zip(m_l1_j2,m_l2_j1))]).reshape(len(m_l1_j1), 2, 2)[np.arange(len(min_m2_pos)),min_m2_pos])        

    return mass_ll_i, mlj

good_charge_lab = ['-1.0e_1.0e_', '1.0e_-1.0e_', '-1.0e_1.0mu', '1.0e_-1.0mu', '_-1.0mu_1.0mu', '_1.0mu_-1.0mu']
mass_ll_select_f = ['el', 'el', 'elmu', 'elmu', 'mu', 'mu']

join_e = np.vectorize(lambda el: "_".join([str(i) + "e" for i in el]))
join_mu = np.vectorize(lambda el: "_".join([str(i) + "mu" for i in el]))        
        
def main():
    options = options_container()

    root_files = {}


    # root_files[options.samples[0]] = []
    # rootdir_MC = os.path.join(options.dataset_dir, 'MC', options.samples[0])
    # for root, subdirs, files in os.walk(rootdir_MC):
    #     for filename in files:
    #         root_files[options.samples[0]].append(os.path.join(root, filename))

    # root_files['data'] = []
    # rootdir_DATA = os.path.join(options.dataset_dir, 'DATA')
    # for root, subdirs, files in os.walk(rootdir_DATA):
    #     for filename in files:
    #         root_files['data'].append(os.path.join(root, filename))

    #compute weigths for MC
    root_files = [
        u'root://eosatlas.cern.ch:1094//eos/atlas/atlasscratchdisk/rucio/user/mcristof/27/e2/user.mcristof.21044190._000001.output.root']

    print('extract pt')

    for path, file, start, stop, data in uproot.iterate(root_files, 'nominal', ['jet_pt'], reportpath=True, reportfile=True, reportentries=True):#
        print(time.time() - start_time_t, file,  start, stop)
        start_time_t = time.time()
        enc_var = 'jet_pt'.encode()
        els = data[enc_var]

    print(len(els))


    # histogram_MC = {}

    # for path, file, start, stop, data in uproot.iterate(root_files[options.samples[0]], 'nominal', options.variables_for_plots + options.variables_for_weigth + options.variable_for_selection, reportpath=True, reportfile=True, reportentries=True):    
    #     print('MC:  ', path, file, start, stop)

    #     enc_var = 'el_charge'.encode()
    #     els = data[enc_var]
    #     if len(els)>0:
    #         enc_var = 'mu_charge'.encode()
    #         muons = data[enc_var]
    #         je = join_e(els)
    #         jmu = join_mu(muons)
    #         charge_e_mu = [i + '_' + j for i,j in (zip(je,jmu))]    

    #         good_evs = np.array([], dtype=int)
    #         mass_ll = np.zeros(len(je))
    #         mass_lj = np.zeros((len(je), 2))
    #         for i,el in enumerate(good_charge_lab):
    #             index = np.where(np.array(charge_e_mu) == el)[0]
    #             index = list(set(np.where(length_checker(data['jet_pt'.encode()]) == 2)[0]).intersection(set(index)))
    #             index = np.array(index)
    #             mass_ll[index], mass_lj[index]= compute_mll(mass_ll_select_f[i], data, index)
    #             index = index[(mass_ll[index] > options.m_ll_CutValue)]
    #             index = index[(mass_lj[index][:,0] >= options.min_m_jl_CutValue) & (mass_lj[index][:,1] >= options.min_m_jl_CutValue)]
    #             index = index[(mass_lj[index][:,0] < options.m_jl_CutValue) & (mass_lj[index][:,1] < options.m_jl_CutValue)]

    #             good_evs = np.concatenate([good_evs, index])

    #         weight = options.lumi * data[b'weight_mc'].flatten() * data[b'weight_pileup'].flatten() * data[b'weight_leptonSF'].flatten() * data[b'weight_jvt'].flatten() * effectiveXs / sumOfWeights;
    #         weight = weight[good_evs]

    #         # accumulate results
    #         for var in options.variables_for_plots:
    #             enc_var = var.encode()
    #             data_var = data[enc_var][good_evs]
    #             arr_len = length_checker(data_var) 
    #             if var == 'jet_pt':
    #                 hist_sf = 0.001
    #             else:
    #                 hist_sf = 1
    #             counts, edges = np.histogram(data_var.flatten() * hist_sf, weights=np.repeat(weight, arr_len), bins=hist_ranges.loc[var, 'nbins'], range=(hist_ranges.loc[var, 'min'], hist_ranges.loc[var, 'max']))
    #             if var not in histogram_MC.keys():
    #                 histogram_MC[var] = counts, edges
    #             else:
    #                 histogram_MC[var] = histogram_MC[var][0] + counts, edges  


    # histogram_DATA = {}

    # for path, file, start, stop, data in uproot.iterate(root_files['data'], 'nominal', options.variables_for_plots + options.variable_for_selection, reportpath=True, reportfile=True, reportentries=True):
    #     print('DATA:  ', path, file, start, stop)

    #     # cut on the charge
    #     enc_var = 'el_charge'.encode()
    #     els = data[enc_var]
    #     if len(els) > 0:
    #         enc_var = 'mu_charge'.encode()
    #         muons = data[enc_var]
    #         je = join_e(els)
    #         jmu = join_mu(muons)
    #         charge_e_mu = [i + '_' + j for i,j in (zip(je,jmu))]    

    #         good_evs = np.array([], dtype=int)
    #         mass_ll = np.zeros(len(je))
    #         mass_lj = np.zeros((len(je), 2))
    #         for i,el in enumerate(good_charge_lab):
    #             index = np.where(np.array(charge_e_mu) == el)[0]
    #             index = list(set(np.where(length_checker(data['jet_pt'.encode()]) == 2)[0]).intersection(set(index)))
    #             index = np.array(index)
    #             mass_ll[index], mass_lj[index]= compute_mll(mass_ll_select_f[i], data, index)
    #             index = index[(mass_ll[index] > options.m_ll_CutValue)]
    #             index = index[(mass_lj[index][:,0] >= options.min_m_jl_CutValue) & (mass_lj[index][:,1] >= options.min_m_jl_CutValue)]

    #             good_evs = np.concatenate([good_evs, index])    

    #         for var in options.variables_for_plots:       
    #             enc_var = var.encode()
    #             data_var = data[enc_var][good_evs]        
    #             if var == 'jet_pt':
    #                 hist_sf = 0.001
    #             else:
    #                 hist_sf = 1
    #             counts, edges = np.histogram(data_var.flatten() * hist_sf, bins=hist_ranges.loc[var, 'nbins'], range=(hist_ranges.loc[var, 'min'], hist_ranges.loc[var, 'max']))
    #             if var not in histogram_DATA.keys():
    #                 histogram_DATA[var] = counts, edges
    #             else:
    #                 histogram_DATA[var] = histogram_DATA[var][0] + counts, edges  

    # print('write hists')
    # output_root_file = uproot.recreate("hist_data_MC_comp.root", compression=uproot.ZLIB(4))
    # for var in options.variables_for_plots:
    #     output_root_file[var + '_MC'] = MakeTH1(hist_ranges.loc[var, 'min'], hist_ranges.loc[var, 'max'], histogram_MC[var][0])
    #     output_root_file[var + '_DATA'] = MakeTH1(hist_ranges.loc[var, 'min'], hist_ranges.loc[var, 'max'], histogram_DATA[var][0])
    # output_root_file.close()


if __name__== "__main__":
    main()
