class options_container:
    def __init__(self):

        self.maxEvents = -1

        # self.dataset_dir = '/Volumes/UNTITLED/AT_OUT'

        self.samples = ['user.mcristof.410472.PhPy8EG.DAOD_FTAG2.e6348_s3126_r9364_p3703.AT_v3_1516_output_root']

        self.tree_name = 'nominal'
        self.xsecs_file_path = 'xsec_ttbar.txt'

        #Luminosities
        self.lumi = 59937.2
        
        
        self.m_jl_CutValue = 175. * 1000
        self.min_m_jl_CutValue = 20. * 1000
        self.m_ll_CutValue = 50. * 1000
        self.cut_pt_l = 28. * 1000
        self.hist_ranges_file = 'hist_ranges.csv'
        
        self.variables_for_weigth = ['weight_mc', 'weight_leptonSF', 'weight_pileup', 'weight_jvt', 'mcChannelNumber']
        self.variable_for_selection = ['el_charge', 'mu_charge', 'el_pt', 'el_eta', 'el_phi', 'mu_pt', 'mu_eta', 'mu_phi', 'jet_pt', 'jet_eta', 'jet_phi', 'jet_e']
        self.variables_for_plots = ["jet_pt", "JetFitter_mass",  "JetFitter_energyFraction",
            "JetFitter_significance3d", "JetFitter_nVTX", "JetFitter_nSingleTracks", "JetFitter_nTracksAtVtx",
            "JetFitter_N2Tpair", "JetFitter_deltaR", "SV1_NGTinSvx", "SV1_masssvx", "SV1_N2Tpair", "SV1_efracsvx",
            "SV1_deltaR", "SV1_Lxy", "SV1_L3d", "SV1_significance3d", "IP2D_bu", "IP2D_bc", "IP2D_cu", "JetFitterSecondaryVertex_nTracks",
            "JetFitterSecondaryVertex_mass", "JetFitterSecondaryVertex_energy", "JetFitterSecondaryVertex_energyFraction",
            "JetFitterSecondaryVertex_displacement3d", "JetFitterSecondaryVertex_displacement2d", "JetFitterSecondaryVertex_maximumTrackRelativeEta",
            "JetFitterSecondaryVertex_minimumTrackRelativeEta", "JetFitterSecondaryVertex_averageTrackRelativeEta",
            "maximumTrackRelativeEta", "minimumTrackRelativeEta", "averageTrackRelativeEta", "JetFitter_deltaphi",
            "JetFitter_deltaeta", "JetFitter_massUncorr", "JetFitter_dRFlightDir", "SV1_dstToMatLay", 
# "softMuon_pt"
# "softMuon_dR"
# "softMuon_eta"
# "softMuon_phi"
# "softMuon_qOverPratio"
# "softMuon_momentumBalanceSignificance"
# "softMuon_scatteringNeighbourSignificance"
# "softMuon_pTrel"
# "softMuon_ip3dD0"
# "softMuon_ip3dZ0"
# "softMuon_ip3dD0Significance"
# "softMuon_ip3dZ0Significance"
# "softMuon_ip3dD0Uncertainty"
# "softMuon_ip3dZ0Uncertainty"
            "SMT_mu_pt", "SMT_mu_d0", "SMT_mu_z0", "MV2c10_discriminant", "MV2r_discriminant", "MV2rmu_discriminant",
            "DL1_pu", "DL1_pc", "DL1_pb", "DL1r_pu", "DL1r_pc", "DL1r_pb", "DL1rmu_pu", "DL1rmu_pc", "DL1rmu_pb", 
            "IP2D_pu", "IP2D_pc", "IP2D_pb", "IP3D_pu", "IP3D_pc", "IP3D_pb", "rnnip_pu", "rnnip_pc", "rnnip_pb", 
            "rnnip_ptau", "jet_eta", "SMT_discriminant", "IP2D_nTrks", "IP3D_nTrks"]